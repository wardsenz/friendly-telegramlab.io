---
title: Friendly Telegram
layout: default
nav_exclude: true
---

# {{ page.title }}

Добро пожаловать в Русскую документацию по юзерботу Friendly-Telegram

Перед установкой юзербота обязательно ознокомьтесь с документацией передвигаясь последовательно по выпадающим меню.

Внимание! Этот сайт не имеет никакого отношения к официальному сайту Friendly-telegram, это всего лишь русская адаптация созданная [мной](https://github.com/mishailovic) и небольшой группой людей для удоюбства использования. Все права принадлежат создателям Friendly-telegram. Оригинальный сайт на английском языке находится [здесь](https://friendly-telegram.gilab.io)

