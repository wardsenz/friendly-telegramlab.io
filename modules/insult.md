---
title: Оскорбления
layout: default
parent: Модули
---

# {{ page.title }}

## Команды

- **Оскорбить кого-то.**
[Syntax: `insult`]
  
  Создает оскорбительное сообщение для человека, которому вы ответили, удобно, когда вы слишком злы, чтобы печатать.
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE0Njc2OTQxMTZdfQ==
-->
