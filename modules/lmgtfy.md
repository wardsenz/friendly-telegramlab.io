---
title: LMGTFY
layout: default
parent: Модули
---

# {{ page.title }}

## Команды

- **Просвещать людей о поисковых системах.**
[Syntax: `lmgtfy <search term>`]
  
  Создает короткую ссылку на мем LMGTFY указанного поискового термина, что удобно, когда глупый человек спамит вам с очевидными вопросами.
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE4ODA0ODYxMzVdfQ==
-->
