---
title: Quick Typer
layout: default
parent: Модули
---

# {{ page.title }}

## Команды

 - **Самоуничтожающееся сообщение.**
[Syntax: `quicktype <x> <сообщение>`]
  
   Создает самоуничтожающееся `<сообщение>`, которое удаляется после `<x>` секунд.
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE2NDAxNTU2MTVdfQ==
-->
